describe("Tickets", ()=> {

    beforeEach(() =>{
        cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html");
    });
    
    it("fills all the text input fields", ()=>{
        const firstName= "Vitor";
        const lastName ="Melo"
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("vitormelo@teste.com.br");
        cy.get("#requests").type("nothing");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });
    
    it("select two tickets", ()=>{
        cy.get("#ticket-quantity").select("2");
    });

    it("select 'VIP' ticket type", ()=>{
        cy.get("#vip").check();
    });

    it("selects 'social media' checkbox", ()=>{
        cy.get("#social-media").check();
    });

    it("select 'friends' and 'publication', then uncheck 'friend'", ()=>{
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    it("has 'TICKETBOX' header's heading", ()=>{
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("Alerts on invalid email", ()=>{
        cy.get("#email")
            .as("email")
            .type("vitor-gmail.com");

        cy.get("#email.invalid")
            .as("invalidEmail")
            .should("exist");

        cy.get("@email")
            .clear()
            .type("vtmelo46@teste.com");

        cy.get("#email.invalid").should("not.exist");
            
    });

    it("fills and reset the form",()=>{
        const firstName= "Vitor";
        const lastName ="Melo"
        const fullName = `${firstName} ${lastName}`;
        const quantity = "2";


        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("vitormelo@teste.com.br");
        cy.get("#requests").type("nothing");
        cy.get("#ticket-quantity").select(quantity);
        cy.get("#vip").check();
        cy.get("#friend").check();

        cy.get(".agreement p").should(
            "contain", 
            `I, ${fullName}, wish to buy ${quantity} VIP tickets.`
        );

        cy.get("#agree").click();
        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");
    })

    it("fills mandatody fields using suport command", ()=>{
        const customer = {
            firstName: "Vitor",
            lastName: "Melo",
            email: "vtmelo46@gmail.com"
        };

        cy.fillMandatoryfields(customer);

        cy.get("button[type='submit']")
        .as("submitButton")
        .should("not.be.disabled");

        cy.get("#agree").uncheck();


        cy.get("@submitButton").should("be.disabled");

    });

});

